package com.example.dome;

import cn.hutool.core.util.StrUtil;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * @author lihanbo
 * @version 1.0
 * @date 2022/4/14 16:19
 */
@MapperScan(basePackages = "com.example.**.mapper")
@SpringBootApplication(scanBasePackages = {"com.example.dome"})
@Slf4j
public class DomeApplication {
    public static void main(String[] args) throws UnknownHostException {
        ConfigurableApplicationContext application = SpringApplication.run(DomeApplication.class, args);
        System.out.println("项目启动成功 *^_^* \n"
                + " .-------.       ____     __        \n"
                + " |  _ _   \\      \\   \\   /  /    \n"
                + " | ( ' )  |       \\  _. /  '       \n"
                + " |(_ o _) /        _( )_ .'         \n"
                + " | (_,_).' __  ___(_ o _)'          \n"
                + " |  |\\ \\  |  ||   |(_,_)'         \n"
                + " |  | \\ `'   /|   `-'  /           \n"
                + " |  |  \\    /  \\      /           \n"
                + " ''-'   `'-'    `-..-'              ");
        Environment env = application.getEnvironment();
        String ip = InetAddress.getLocalHost().getHostAddress();
        String port = env.getProperty("server.port");
        String path = env.getProperty("server.servlet.context-path");
        if (StrUtil.isEmpty(path)) {
            path = "";
        }
        log.info("\n----------------------------------------------------------\n\t"
                + "Application  is running! Access URLs:\n\t"
                + "Local访问网址: \t\thttp://localhost:" + port + path + "\n\t"
                + "External访问网址: \thttp://" + ip + ":" + port + path + "\n\t"
                + "Swagger访问网址: \thttp://" + ip + ":" + port + "/doc.html" + "\n\t"
                + "----------------------------------------------------------");
    }
}
