package com.example.dome.common;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.symmetric.AES;

public class AESUtils {

    //16位密钥
    public final static String key = "mybatisplus88888";

    /**
     * 加密
     */
    public static String encrypt(String data) {
        AES aes = SecureUtil.aes(StrUtil.bytes(key, CharsetUtil.CHARSET_UTF_8));
        byte[] encrypt = aes.encrypt(data);
        return Base64.encode(encrypt);
    }

    /**
     * 解密
     */
    public static String decrypt(String data) {
        AES aes = SecureUtil.aes(StrUtil.bytes(key, CharsetUtil.CHARSET_UTF_8));
        byte[] decrypt = aes.decrypt(Base64.decode(data));
        return StrUtil.str(decrypt, CharsetUtil.CHARSET_UTF_8);
    }

    public static void main(String[] args) {
    }

}
