package com.example.dome.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.dome.entity.User;

/**
 * @author Administrator
 * @description 针对表【sys_user】的数据库操作Mapper
 * @createDate 2022-04-17 08:51:56
 * @Entity generator.domain.User
 */
public interface UserMapper extends BaseMapper<User> {

}




