package com.example.dome.controller;

import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.dome.common.AESUtils;
import com.example.dome.entity.User;
import com.example.dome.mapper.UserMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;


/**
 * @author Administrator
 */
@Api(tags = "test")
@RestController
@AllArgsConstructor
@Slf4j
public class TestController {

    private final UserMapper userMapper;

    @ApiOperation("添加")
    @PostMapping(value = "/add")
    public Object add() {
        User user = new User();
        user.setId(IdUtil.getSnowflakeNextId());
        user.setCreateDate(new Date());
        user.setUsername(IdUtil.simpleUUID());
        //AES加密， mysql sql中可以使用【 AES_DECRYPT(from_base64(`mobile`),'aes密钥Key') 】进行字段解密后查询
        user.setMobile("1234567890");
        userMapper.insert(user);
        return user;
    }

    @ApiOperation("查询,加密字段搜索")
    @PostMapping(value = "/list")
    public Object list() {
        List<User> list = userMapper.selectList(new QueryWrapper<User>().lambda()
                .apply("AES_DECRYPT(from_base64(`mobile`),'" + AESUtils.key + "') like '%1234%'"));
        return list;
    }

    @ApiOperation("详情")
    @PostMapping(value = "/info")
    public Object info() {
        User user = userMapper.selectById("1515664005515571200");
        return user;
    }
}
