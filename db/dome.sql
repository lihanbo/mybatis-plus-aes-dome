/*
 Navicat Premium Data Transfer

 Source Server         : 本机mysql
 Source Server Type    : MySQL
 Source Server Version : 80015
 Source Host           : localhost:3306
 Source Schema         : dome

 Target Server Type    : MySQL
 Target Server Version : 80015
 File Encoding         : 65001

 Date: 18/04/2022 10:52:39
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` bigint(20) NOT NULL,
  `create_id` bigint(20) NULL DEFAULT NULL COMMENT '操作者',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_id` bigint(20) NULL DEFAULT NULL COMMENT '修改者',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `delete_state` tinyint(1) NULL DEFAULT NULL COMMENT '删除状态 0正常 1删除',
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `mobile` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号AES加密存储',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1515664005515571200, NULL, '2022-04-17 20:10:33', NULL, NULL, NULL, '1d58d77dd09345cebfee3efd00947a9c', 'bBjMwfjc8s86+S9/HJILKA==');
INSERT INTO `user` VALUES (1515668721741869056, NULL, '2022-04-17 20:29:18', NULL, NULL, NULL, '74c536c9c1f445ac9907df7a4ba0737c', 'bBjMwfjc8s86+S9/HJILKA==');

SET FOREIGN_KEY_CHECKS = 1;
